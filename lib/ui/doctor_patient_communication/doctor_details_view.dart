import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/doctor_patient_communication/patient_appointment/create_patient_appointment.dart';
import 'package:skip_the_clinic/ui/doctor_patient_communication/patient_inquiry/create_patient_inquiry.dart';

class DoctorDetailsView extends StatelessWidget {
  final String name;
  final String picture;

  DoctorDetailsView(this.name, this.picture);
  var subtitleTextStyle = TextStyle(fontSize: 20,fontWeight: FontWeight.w300);
  var titleTextStyle = TextStyle(fontSize: 30,fontWeight: FontWeight.w500);
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text('Dr. $name'),
        ),
        body: SafeArea(child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
          Container(
            width: double.infinity,
              height: 200,
              child: Image.network("https://picsum.photos/500",width: double.infinity,fit: BoxFit.cover,)),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
            Text('Contact Dr. $name',style: titleTextStyle,),
            Text('Dr. $name has an MBBS & MD. Specializes in Oncology, ENT, Toxicology and Cardiology',style: subtitleTextStyle,),
              SizedBox(height: screenSize*0.1,),
              ElevatedButton(onPressed: (){

                Navigator.of(context).push(
                  MaterialPageRoute(
                    settings: RouteSettings(name: "/CreatePatientInquiry"),
                    builder: (context) => CreatePatientInquiry(name, picture),
                  ),
                );
              }, child: Text('Send Inquiry'),),
              ElevatedButton(onPressed: (){

                Navigator.of(context).push(
                  MaterialPageRoute(
                    settings: RouteSettings(name: "/CreatePatientAppointment"),
                    builder: (context) => CreatePatientAppointment(name, picture),
                  ),
                );
              }, child: Text('Create an Appointment'),),
          ],),),


        ],),));
  }
}
