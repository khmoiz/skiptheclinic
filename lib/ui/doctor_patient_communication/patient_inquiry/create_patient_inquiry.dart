import 'package:flutter/material.dart';

class CreatePatientInquiry extends StatelessWidget {
  final String name;
  final String picture;

  CreatePatientInquiry(this.name, this.picture);
  var subtitleTextStyle = TextStyle(fontSize: 25,fontWeight: FontWeight.w300);

  var titleTextStyle = TextStyle(fontSize: 35,fontWeight: FontWeight.w500);

  var listSubtitleTextStyle = TextStyle(fontSize: 15,fontWeight: FontWeight.w300);

  var listTitleTextStyle = TextStyle(fontSize: 25,fontWeight: FontWeight.w500);

  final _formKey = GlobalKey<FormState>();
  final inquiryTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Inquiry for Dr $name'),
        ),
        body: SafeArea(child: ContentBodyWidget(formKey: _formKey, titleTextStyle: titleTextStyle, inquiryTextController: inquiryTextController, name: name, listSubtitleTextStyle: listSubtitleTextStyle, screenSize: screenSize),));
  }
}

class ContentBodyWidget extends StatefulWidget {
  const ContentBodyWidget({
    Key key,
    @required GlobalKey<FormState> formKey,
    @required this.titleTextStyle,
    @required this.inquiryTextController,
    @required this.name,
    @required this.listSubtitleTextStyle,
    @required this.screenSize,
  }) : _formKey = formKey, super(key: key);

  final GlobalKey<FormState> _formKey;
  final TextStyle titleTextStyle;
  final TextEditingController inquiryTextController;
  final String name;
  final TextStyle listSubtitleTextStyle;
  final double screenSize;

  @override
  _ContentBodyWidgetState createState() => _ContentBodyWidgetState();
}

class _ContentBodyWidgetState extends State<ContentBodyWidget> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget._formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text('Create Patient Inquiry',style: widget.titleTextStyle,textAlign: TextAlign.center,),
        ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: widget.inquiryTextController,
              minLines: 6,
              maxLines: 8,
              keyboardType: TextInputType.multiline,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter your inquiry';
                } else if (value.split(" ").length < 20) {
                  return 'Your inquiry cannot be less than 20 words!';
                }
                return null;
              },
              decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green, width: 2),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 2),
                  ),

                  helperText: "What should Dr.${widget.name} be informed of?",
                labelText: "Inquiry Field",
                hintText: "I'm feeling very dizzy and have alot of aches in my joint. This started last week...",
                hintStyle: widget.listSubtitleTextStyle
                  )
            ),
          ),
          SizedBox(height:widget.screenSize*0.025 ,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: ElevatedButton(onPressed: (){

    if (widget._formKey.currentState.validate()) {


      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Inquiry Sent!",textAlign: TextAlign.center),
            content: new Text("Your Inquiry was sent to Dr.${widget.name}. Please give the Doctor at-least 24 hours to respond.",textAlign: TextAlign.center,),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Great!"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      ).then((value) => { Navigator.of(context)
          .popUntil(ModalRoute.withName("/HomeView"))});
    }

    }, child: Text('Send Inquiry'),
              ),
          )

          ],),
    );
  }
}
