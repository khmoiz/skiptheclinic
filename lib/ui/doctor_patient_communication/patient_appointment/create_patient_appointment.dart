import 'package:flutter/material.dart';

class CreatePatientAppointment extends StatefulWidget {
  final String name;
  final String picture;

  CreatePatientAppointment(this.name, this.picture);

  @override
  _CreatePatientAppointmentState createState() => _CreatePatientAppointmentState();
}

class _CreatePatientAppointmentState extends State<CreatePatientAppointment> {
  var subtitleTextStyle = TextStyle(fontSize: 25,fontWeight: FontWeight.w300);

  var titleTextStyle = TextStyle(fontSize: 35,fontWeight: FontWeight.w500);

  var listSubtitleTextStyle = TextStyle(fontSize: 15,fontWeight: FontWeight.w300);

  var listTitleTextStyle = TextStyle(fontSize: 25,fontWeight: FontWeight.w500);

  final duplicateItems = ["12:30pm - 1:30pm","1:45pm - 2:30pm","3:00pm - 3:30pm"," 4:00pm - 5:00pm", "6:00pm - 6:30pm","8:30pm - 9:30pm"];

  List<String> _dates = ['20/11/20','21/11/20','22/11/20','23/11/20','24/11/20','25/11/20','27/11/20', '28/11/20', '29/11/20', '30/11/20'];

  String _selectedDate;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text('Create an Appointment'),
        ),
        body: SafeArea(child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
            SizedBox(height: screenSize * 0.1 ,),
            Text('Choose a Time',style: titleTextStyle,textAlign: TextAlign.center,),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Flex(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                Image.network(widget.picture,height: 100,fit: BoxFit.contain,),
                Text('Dr.${widget.name}',style: subtitleTextStyle,textAlign: TextAlign.center),

              ],direction: Axis.horizontal,),
            ),
            Text('Please select the appropriate day and time from the time slots available below.',style: subtitleTextStyle,textAlign: TextAlign.center),
              SizedBox(height: screenSize * 0.025 ,),
              Center(
                child: DropdownButton(
                  hint: Text('Please choose a Date'), // Not necessary for Option 1
                  value: _selectedDate,
                  onChanged: (newValue) {
                    setState(() {
                      _selectedDate = newValue;
                    });
                  },
                  items: _dates.map((date) {
                    return DropdownMenuItem(
                      child: new Text(date,style: subtitleTextStyle,textAlign: TextAlign.center,),
                      value: date,
                    );
                  }).toList(),
                ),
              ),
              SizedBox(height: screenSize * 0.025 ,),
              Divider(height: 20,color: Colors.grey,thickness: 2.0,indent: 20.0,endIndent: 20.0,),
              Expanded(
                child: ListView.builder(
                  itemCount: duplicateItems.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: Icon(Icons.more_time_rounded),
                      title: Text('${duplicateItems[index]}',style: listTitleTextStyle,),
                      subtitle: Text('Select this slot for booking',style: listSubtitleTextStyle,),
                      trailing: OutlinedButton(onPressed: (){

                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: new Text("Appointment Created!",textAlign: TextAlign.center),
                              content: new Text("Your Appointment is set with Dr.${widget.name} at ${duplicateItems[index]}!",textAlign: TextAlign.center,),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Great!"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        ).then((value) => { Navigator.of(context)
                            .popUntil(ModalRoute.withName("/HomeView"))});


                      }, child: Text('Book',style: listSubtitleTextStyle.copyWith(fontWeight: FontWeight.bold),),
                      ),
                    );
                  },
                ),
              )
            ],),));
  }
}
