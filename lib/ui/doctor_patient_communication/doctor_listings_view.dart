import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/doctor_patient_communication/doctor_details_view.dart';



class DoctorListingsView extends StatefulWidget {
  DoctorListingsView({Key key}) : super(key: key);

  @override
  _DoctorListingsViewState createState() => _DoctorListingsViewState();
}

class _DoctorListingsViewState extends State<DoctorListingsView> {
  TextEditingController editingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;
    var subtitleTextStyle = TextStyle(fontSize: 15,fontWeight: FontWeight.w300);
    var titleTextStyle = TextStyle(fontSize: 24,fontWeight: FontWeight.w500);

    var items = [];

    final duplicateItems = ["Woodrow Carney",
      "Lyndsey Mcmanus",
      "Archibald Patrick",
      "Isla-Mae Gutierrez",
      "Susanna Cano",
      "Alexia Kenny",
      "Cano Alexis",
      "Bruce Wayne","Clark Kent",
      "Alan Scott",
      "Teagan Brooks"];



    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Find a Doctor'),
        ),
        body: SafeArea(child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: editingController,
                  decoration: InputDecoration(
                      labelText: "Search for a Doctor",
                      hintText: "eg. John Dorian",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)))),
                ),
              ),
          Expanded(
            child: ListView.builder(
            itemCount: duplicateItems.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: CircleAvatar(backgroundImage: NetworkImage("https://picsum.photos/500"),radius: 34,),
                  title: Text('${duplicateItems[index]}',style: titleTextStyle,),
                  subtitle: Text('Dr. ${duplicateItems[index]} MD MBBS',style: subtitleTextStyle,),
                  trailing: IconButton(icon: Icon(Icons.double_arrow),iconSize:36,onPressed: (){

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        settings: RouteSettings(name: "/DoctorDetailsView"),
                        builder: (context) => DoctorDetailsView(duplicateItems[index], "https://picsum.photos/200"),
                      ),
                    );
                  }),
                      );
              },
            ),
          )
          ],),
        ),));

  }


}