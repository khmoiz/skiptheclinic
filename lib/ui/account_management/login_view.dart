import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/landingPage/home_view.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login Page'),
        ),
        body: SingleChildScrollView(child: LoginViewForm()));
  }
}

class LoginViewForm extends StatelessWidget {
  const LoginViewForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    final emailTextController = TextEditingController();
    final passwordTextController = TextEditingController();
    var screenSize = MediaQuery.of(context).size.height;

    var textFieldHeadingTextStyle = TextStyle(fontSize: 24);

    return Column(
      children: [
        //10% of Screen Height
        SizedBox(
          height: screenSize * 0.1,
        ),
        Form(
          key: _formKey,
          child: Container(
            height: screenSize * 0.5,
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text('Email:', style: textFieldHeadingTextStyle),
                TextFormField(
                  controller: emailTextController,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter your Email!';
                    } else if (!validateEmail(value)) {
                      return 'Please enter a Valid Email (doctor@doctor.com)';
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      hintText: 'Email',
                      helperText: "Please enter your Patient or Doctor Email.",
                      icon: Icon(Icons.contact_mail_outlined)),
                ),
                Text('Password:', style: textFieldHeadingTextStyle),
                TextFormField(
                  controller: passwordTextController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter your password';
                    } else if (value.length < 4) {
                      return 'Your password must be longer than 3 Digits!';
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      hintText: 'Password',
                      helperText: "Please enter your 4 digit password.",
                      icon: Icon(Icons.lock_open_outlined)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      print("~~~~~~~~~~~~~${emailTextController.text.toString()}~~~~~~~~~~~~~~");

                      if (_formKey.currentState.validate()) {
                        print("~~~~~~~~~~~~~${emailTextController.text.toString()}~~~~~~~~~~~~~~");

                        if (emailTextController.text.toString() == "doctor@d.com") {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              settings: RouteSettings(name: "/HomeView"),
                              builder: (context) => HomeView("medical"),
                            ),
                          );
                        } else if (emailTextController.text.toString() == "patient@p.com") {

                          Navigator.of(context).push(
                            MaterialPageRoute(
                              settings: RouteSettings(name: "/HomeView"),
                              builder: (context) => HomeView("patient"),
                            ),
                          );
                        } else {
                          print("~~~~~~~~~~~~~${emailTextController.text.toString()}~~~~~~~~~~~~~~");
                          Scaffold.of(context).showSnackBar(SnackBar(
                              duration: Duration(seconds: 3),
                              content: Text('Sorry, User with the email ${emailTextController.text.toString()} not found!')));
                        }
                      }
                    },
                    child: Text('Login'),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

bool validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  return (!regex.hasMatch(value)) ? false : true;
}
