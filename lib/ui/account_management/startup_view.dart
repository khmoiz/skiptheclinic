import 'package:flutter/material.dart';

import 'login_view.dart';

class StartupView extends StatefulWidget {
  StartupView({Key key}) : super(key: key);

  @override
  _StartupViewState createState() => _StartupViewState();
}

class _StartupViewState extends State<StartupView> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        // appBar: AppBar(
        //   title: Text('${widget.title}'),
        // ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                Center(child: Text('Skip The Clinic',style: TextStyle(fontSize: 40),)),
                SizedBox(
                  height: screenSize * 0.15,
                ),
                ElevatedButton(onPressed: (){

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      settings: RouteSettings(name: "/LoginView"),
                      builder: (context) => LoginView(),
                    ),
                  );
                }, child: Text('Login'),),
                SizedBox(
                  height: screenSize * 0.05,
                ),
                ElevatedButton(onPressed: (){}, child: Text('Create an Account'),),
              ],
            ),
          ),
        ));
  }
}
