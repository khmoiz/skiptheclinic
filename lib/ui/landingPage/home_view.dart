import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/doctor_patient_communication/doctor_listings_view.dart';
import 'package:skip_the_clinic/ui/perscriptions/patient/patient_prescriptions_view.dart';

import '../health_report/patient/symptom_list_view.dart';

class HomeView extends StatelessWidget {
  final String user;

  HomeView(this.user);

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Home'),
        ),
        body: Container(
            height: screenSize * 0.5,
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: (user == "patient" ? PatientView() : MedicalStaffView())));
  }
}

class PatientView extends StatelessWidget {
  const PatientView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                settings: RouteSettings(name: "/SymptomListView"),
                builder: (context) => SymptomListView(),
              ),
            );
          },
          child: Text('Create Health Report'),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                settings: RouteSettings(name: "/DoctorListingsView"),
                builder: (context) => DoctorListingsView(),
              ),
            );
          },
          child: Text('Communicate with Doctor'),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                settings: RouteSettings(name: "/UserPrescriptionsView"),
                builder: (context) => UserPrescriptionsView(),
              ),
            );
          },
          child: Text('View my Prescriptions'),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Manage my Account'),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Sign Out'),
        ),
      ],
    );
  }
}

class MedicalStaffView extends StatelessWidget {
  const MedicalStaffView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ElevatedButton(
          onPressed: () {},
          child: Text('View Patient Health Reports'),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Communicate with Patient'),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Manage my Account'),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Sign Out'),
        ),
      ],
    );
  }
}
