import 'package:flutter/material.dart';

class MedicationDetailsView extends StatelessWidget {
  final String medicationName;

  MedicationDetailsView(this.medicationName);
  var subtitleTextStyle = TextStyle(fontSize: 20,fontWeight: FontWeight.w300);
  var titleTextStyle = TextStyle(fontSize: 30,fontWeight: FontWeight.w500);
  var mediumTitleTextStyle = TextStyle(fontSize: 35,fontWeight: FontWeight.w400,fontStyle: FontStyle.italic);
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text('$medicationName'),
        ),
        body: SafeArea(child:
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: screenSize*0.05,),
              Text('$medicationName',style: mediumTitleTextStyle,),
              SizedBox(height: screenSize*0.025,),
              Text('Details:',style: titleTextStyle,),
              SizedBox(height: screenSize*0.025,),

              Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                  ' Duis mattis a felis in scelerisque. Morbi fringilla justo ac odio consequat ornare. Aliquam feugiat erat vitae mi vulputate condimentum. Nulla et '
                  'tempus augue. Aenean a augue blandit, molestie ante non, facilisis dolor. Nulla quis elit ut ex maximus facilisis at ut.',style: subtitleTextStyle,),
            ],),
        ),));
  }
}
