import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/landingPage/home_view.dart';

class RecordCustomNotesView extends StatelessWidget {
  var subtitleTextStyle = TextStyle(fontSize: 22, fontWeight: FontWeight.w300);
  var titleTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.w500);
  var mediumTitleTextStyle =
      TextStyle(fontSize: 25, fontWeight: FontWeight.w300);

  final _formKey = GlobalKey<FormState>();
  final customNotesTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: AppBar(
          title: Text('Anything Extra to Add?'),
        ),
        bottomNavigationBar: Container(
          height: 50,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () {
              /* Navigator.of(context).push(
                MaterialPageRoute(
                  settings: RouteSettings(name: "/RecordCustomNotesView"),
                  builder: (context) => RecordCustomNotesView(),
                ),
              );*/
              Navigator.of(context).push(
                MaterialPageRoute(
                  settings: RouteSettings(name: "/HomeView"),
                  builder: (context) => HomeView("patient"),
                ),
              );
            },
            child: Text('Finish Health Report'),
          ),
        ),
        body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: screenHeight * 0.025,
              ),
              Text(
                'Please add any extra notes below.',
                textAlign: TextAlign.center,
                style: mediumTitleTextStyle,
              ),
              SizedBox(
                height: screenHeight * 0.05,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      'Create Custom Notes',
                      style: titleTextStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                        controller: customNotesTextController,
                        minLines: 6,
                        maxLines: 8,
                        keyboardType: TextInputType.multiline,
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.green, width: 2),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 2),
                          ),
                          helperText: "What extra would you like to Record?",
                          labelText: "Custom Notes",
                          hintText:
                              "I'm feeling very dizzy and have alot of aches in my joint. This started last week...",
                        )),
                  ),
                  SizedBox(
                    height: screenHeight * 0.025,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
