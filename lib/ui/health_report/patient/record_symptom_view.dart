import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skip_the_clinic/ui/health_report/patient/record_medication_view.dart';

var subtitleTextStyle = TextStyle(fontSize: 22, fontWeight: FontWeight.w300);
var titleTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.w500);
var mediumTitleTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.w400);

class RecordSymptomView extends StatefulWidget {
  @override
  _RecordSymptomViewState createState() => _RecordSymptomViewState();
}

class _RecordSymptomViewState extends State<RecordSymptomView> {
  final symptomsList = [
    "Aches and pains",
    "Sore throat",
    "Diarrhoea",
    "Conjunctivitis",
    "Headache",
    "Loss of taste or smell",
    "Rash on skin, or discolouration of fingers or toes",
    "Fever",
    "Dry cough",
    "Tiredness",
  ];

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text('Symptom Severity'),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: ListView(
              children: [
                Text(
                  'Please Describe Symptom Details below..',
                  textAlign: TextAlign.center,
                  style: mediumTitleTextStyle,
                ),
                SizedBox(
                  height: screenSize * 0.005,
                ),
                SymptomBox("Diarrhoea", 5.0),
                SizedBox(
                  height: screenSize * 0.005,
                ),
                SymptomBox("Fever", 8.0),
                SizedBox(
                  height: screenSize * 0.005,
                ),
                SymptomBox("Loss of taste or smell", 4.0),
                SizedBox(
                  height: screenSize * 0.025,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        settings: RouteSettings(name: "/RecordMedicationView"),
                        builder: (context) => RecordMedicationView(),
                      ),
                    );
                  },
                  child: Container(
                      height: 50, child: Center(child: Text('Confirm'))),
                )
              ],
            ),
          ),
        ));
  }
}

class SymptomBox extends StatefulWidget {
  final String symptomName;
  final double symptomLevel;

  SymptomBox(this.symptomName, this.symptomLevel);

  @override
  _SymptomBoxState createState() => _SymptomBoxState();
}

class _SymptomBoxState extends State<SymptomBox> {
  String formattedDate = DateFormat.yMd().add_jm().format(DateTime.now());
  double _currentSliderValue = 1;
  @override
  void initState() {
    super.initState();
    _currentSliderValue = widget.symptomLevel;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: double.infinity,
        height: 150,
        color: Colors.grey[400],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              '${widget.symptomName}',
              style: mediumTitleTextStyle,
              textAlign: TextAlign.center,
            ),
            Slider(
              value: _currentSliderValue,
              min: 1,
              max: 10,
              divisions: 10,
              label: _currentSliderValue.round().toString(),
              onChanged: (double value) {
                setState(() {
                  _currentSliderValue = value.round().toDouble();
                });
              },
            ),
            Text(
              'Current Severity: $_currentSliderValue',
              style: subtitleTextStyle,
              textAlign: TextAlign.center,
            ),
            Text(
              '$formattedDate',
              style: subtitleTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
