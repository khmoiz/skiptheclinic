import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skip_the_clinic/ui/health_report/patient/record_custom_notes_view.dart';

var subtitleTextStyle = TextStyle(fontSize: 22, fontWeight: FontWeight.w300);
var titleTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.w500);
var mediumTitleTextStyle = TextStyle(fontSize: 25, fontWeight: FontWeight.w300);

class RecordMentalHealthView extends StatefulWidget {
  @override
  _RecordMentalHealthViewState createState() => _RecordMentalHealthViewState();
}

class _RecordMentalHealthViewState extends State<RecordMentalHealthView> {
  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    String formattedDate = DateFormat.yMd().add_jm().format(DateTime.now());

    List<String> _moods = [
      "Cheerful",
      "Reflective",
      "Gloomy",
      "Humorous",
      "Melancholy",
      "Idyllic",
      "Whimsical",
      "Romantic",
      "Mysterious",
      "Ominous",
      "Calm",
      "Lighthearted",
      "Hopeful",
      "Angry",
      "Fearful",
      "Tense",
      "Lonely",
    ];

    String _selectedMoods;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Your Mental Health'),
        ),
        bottomNavigationBar: Container(
          height: 50,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  settings: RouteSettings(name: "/RecordCustomNotesView"),
                  builder: (context) => RecordCustomNotesView(),
                ),
              );
            },
            child: Text('Add Extra Notes'),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 25,
            ),
            child: ListView(
              children: [
                SizedBox(
                  height: screenHeight * 0.025,
                ),
                Text(
                  'Your Mental Health is important, please take a minute to reflect.',
                  textAlign: TextAlign.center,
                  style: mediumTitleTextStyle,
                ),
                SizedBox(
                  height: screenHeight * 0.05,
                ),
                MentalHealthBox("Stress", 9.0),
                SizedBox(
                  height: screenHeight * 0.025,
                ),
                MentalHealthBox("Anxiety", 6.0),
                SizedBox(
                  height: screenHeight * 0.025,
                ),
                Center(
                  child: Container(
                    width: double.infinity,
                    height: 150,
                    color: Colors.grey[400],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Current Mood',
                          style: mediumTitleTextStyle,
                          textAlign: TextAlign.center,
                        ),
                        DropdownButton(
                          hint: Text(
                              'Please choose your Mood'), // Not necessary for Option 1
                          value: _selectedMoods,
                          onChanged: (newValue) {
                            setState(() {
                              _selectedMoods = newValue;
                            });
                          },
                          items: _moods.map((mood) {
                            return DropdownMenuItem(
                              child: new Text(
                                mood,
                                style: subtitleTextStyle,
                                textAlign: TextAlign.center,
                              ),
                              value: mood,
                            );
                          }).toList(),
                        ),
                        Text(
                          'Your Mood is $_selectedMoods',
                          style: subtitleTextStyle,
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          '$formattedDate',
                          style: subtitleTextStyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

class MentalHealthBox extends StatefulWidget {
  final String symptomName;
  final double symptomLevel;

  MentalHealthBox(this.symptomName, this.symptomLevel);

  @override
  _MentalHealthBoxState createState() => _MentalHealthBoxState();
}

class _MentalHealthBoxState extends State<MentalHealthBox> {
  String formattedDate = DateFormat.yMd().add_jm().format(DateTime.now());
  double _currentSliderValue = 1;

  @override
  void initState() {
    super.initState();
    _currentSliderValue = widget.symptomLevel;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: double.infinity,
        height: 150,
        color: Colors.grey[400],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              '${widget.symptomName}',
              style: mediumTitleTextStyle,
              textAlign: TextAlign.center,
            ),
            Slider(
              value: _currentSliderValue,
              min: 1,
              max: 10,
              divisions: 10,
              label: _currentSliderValue.round().toString(),
              onChanged: (double value) {
                setState(() {
                  _currentSliderValue = value.round().toDouble();
                });
              },
            ),
            Text(
              'Current Severity: $_currentSliderValue',
              style: subtitleTextStyle,
              textAlign: TextAlign.center,
            ),
            Text(
              '$formattedDate',
              style: subtitleTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
