import 'package:flutter/material.dart';
import 'package:skip_the_clinic/ui/health_report/medication_details_view.dart';

import 'record_mental_health_view.dart';

class RecordMedicationView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController editingController = TextEditingController();

    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    var subtitleTextStyle =
        TextStyle(fontSize: 15, fontWeight: FontWeight.w300);
    var titleTextStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.w500);

    final medicationList = [
      "Simvastatin",
      "Vicodin",
      "Lisinopril",
      "Tylenol",
      "Pepto-Bismol",
      "Asprin",
      "Advil",
      "Ibuprofen",
      "Valacyclovir",
    ];

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Create a Health Report'),
        ),
        bottomNavigationBar: Container(
          height: 50,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  settings: RouteSettings(name: "/RecordMentalHealthView"),
                  builder: (context) => RecordMentalHealthView(),
                ),
              );
            },
            child: Text('Next'),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: editingController,
                    decoration: InputDecoration(
                        labelText: "Search for a Medication",
                        hintText: "eg. Panadol / Paracetamol",
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)))),
                  ),
                ),
                Wrap(
                  alignment: WrapAlignment.spaceEvenly,
                  direction: Axis.horizontal,
                  children: [
                    Divider(
                      thickness: 2.0,
                      endIndent: 10,
                      indent: 10,
                      color: Colors.grey,
                    ),
                    Text('Please long press the medication for more details'),
                    Divider(
                      thickness: 2.0,
                      endIndent: 10,
                      indent: 10,
                      color: Colors.grey,
                    )
                  ],
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: medicationList.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(
                          '${medicationList[index]}',
                          style: titleTextStyle,
                        ),
                        trailing: Container(
                          width: 150,
                          child: CheckboxListTile(
                            title: Text(
                              "Active",
                              style: subtitleTextStyle,
                            ),

                            value: false,
                            onChanged: null, //  <-- leading Checkbox
                          ),
                        ),
                        onLongPress: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              settings:
                                  RouteSettings(name: "/MedicationDetailsView"),
                              builder: (context) =>
                                  MedicationDetailsView(medicationList[index]),
                            ),
                          );
                        },
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
