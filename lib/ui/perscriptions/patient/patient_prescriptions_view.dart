import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skip_the_clinic/ui/perscriptions/patient/patient_prescription_details.dart';

class UserPrescriptionsView extends StatefulWidget {
  UserPrescriptionsView({Key key}) : super(key: key);

  @override
  _UserPrescriptionsViewState createState() => _UserPrescriptionsViewState();
}

class _UserPrescriptionsViewState extends State<UserPrescriptionsView> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;
    var subtitleTextStyle =
        TextStyle(fontSize: 15, fontWeight: FontWeight.w300);
    var titleTextStyle = TextStyle(fontSize: 24, fontWeight: FontWeight.w500);

    var items = [];
    var now = new DateTime.now();

    final duplicateItems = [
      "Lisinopril",
      "Valacyclovir",
      "Metformin",
      "Oxycoton",
      "Metoprolol",
      "Zoloft",
      "Vicodin",
    ];

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Your Prescriptions'),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextField(
                  decoration: InputDecoration(
                      labelText: "Search for a Prescription",
                      hintText: "eg. Anti-Biotic",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0)))),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: duplicateItems.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        trailing: Text(
                            "${DateFormat.MMMEd().format(now.subtract(Duration(days: index)))}"),
                        title: Text(
                          '${duplicateItems[index]}',
                          style: titleTextStyle,
                        ),
                        subtitle: Text(
                          'Assigned by Dr.Marini',
                          style: subtitleTextStyle,
                        ),
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                settings: RouteSettings(
                                    name: "/PatientPrescriptionDetails"),
                                builder: (context) =>
                                    PatientPrescriptionDetails(
                                        duplicateItems[index], index)),
                          );
                        },
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
