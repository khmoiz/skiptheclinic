import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PatientPrescriptionDetails extends StatelessWidget {
  final String name;
  final int index;

  PatientPrescriptionDetails(this.name, this.index);
  var subtitleTextStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.w300);
  var titleTextStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.w500);
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size.height;

    var currentDayData = DateFormat.MMMMEEEEd()
        .format(DateTime.now().subtract(Duration(days: index)));

    return Scaffold(
        appBar: AppBar(
          title: Text('$name'),
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  width: double.infinity,
                  height: 200,
                  child: Image.network(
                    "https://www.madeformedical.com/wp-content/uploads/2018/07/vio-4.jpg",
                    width: double.infinity,
                    fit: BoxFit.cover,
                  )),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      '$name',
                      style: titleTextStyle,
                    ),
                    Text(
                      'The medication $name was assigned by Dr.Marini on the ${currentDayData}',
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: screenSize * 0.05,
                    ),
                    Text(
                      'Reason: For body pain, take 2 times a day after food.',
                      style: subtitleTextStyle,
                    ),
                    SizedBox(
                      height: screenSize * 0.05,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
